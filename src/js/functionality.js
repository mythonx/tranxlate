var selecteds = [],
  selected_import = "",
  steps = {
    import: false,
    destination_files: false,
    one_key: false,
  };
function steps() {
  if (steps["import"]) {
    console.log("true");
  }
}
function add_import() {
  steps["import"] = true;
  selected_import = document.getElementById("base-lang");
  selected_import =
    selected_import.options[selected_import.selectedIndex].value;

  document.getElementById("base-selected").innerHTML = `
        <br />
        <div class="field has-addons">
            <div class="control">
                <input class="input" type="text" value="${selected_import}" disabled>
            </div>
            <p class="control">
                <button class="button is-link" onclick="remove_lang(this)" id="${selected_import}">
                    File
                </button>
            </p>
            <p class="control">
                <button class="button is-danger" onclick="remove_lang(this)" id="">
                    Remove
                </button>
            </p>
        </div>
    `;

  // div["base-selected"]
}

function append_lang() {
  //   document.getElementById("exportBtn").removeAttribute("disabled");
  let select = document.getElementById("language");
  let selectedOne = select.options[select.selectedIndex].value;
  if (!selecteds.includes(selectedOne)) {
    selecteds.push(selectedOne);
  }
  document.getElementById("list-of-selected").innerHTML = "<br />";
  selecteds.forEach((s) => {
    document.getElementById("list-of-selected").innerHTML += `
        <div class="field has-addons">
            <div class="control">
                <input class="input" type="text" value="${s}" disabled>
            </div>
            <p class="control">
                <button class="button is-link" onclick="remove_lang(this)" id="${s}">
                    File
                </button>
            </p>
            <p class="control">
                <button class="button is-danger" onclick="remove_lang(this)" id="${s}">
                    Remove
                </button>
            </p>
        </div>
    `;
  });
}

function remove_lang(buttn) {
  elemValue = buttn.id;
  elemValue.replace(" ", "");
  var index = selecteds.indexOf(elemValue);
  if (index > -1) {
    selecteds.splice(index, 1);
  }
  let elem = buttn.parentNode.parentNode;
  elem.parentNode.removeChild(elem);
}
